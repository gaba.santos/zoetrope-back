const { response } = require('express');
const List = require('../models/List');

const create = async (req,res) => {
    try{
        const list = await List.create(req.body);
        return res.status(201).json({message: "Lista criada!", list: list});
    }catch(err){
        res.status(500).json({error:err});
    }
};

const index = async(req,res) => {
    try{
        const list = await List.findAll();
        return res.status(200).json({lists});
    }catch(err){
            return res.status(500).json({err});
        }

};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await List.update(req.body, {where: {id: id}});
        if(updated) {
            const list = await List.findByPk(id);
            return res.status(200).send(list);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Lista não encontrada");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await List.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Lista deletada!");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Lista não encontrada");
    }
};

module.exports = {
    index,
    create,
    update,
    destroy
};
