const { response } = require('express');
const Criticism = require('../models/Criticism');

const create = async (req,res) => {
    try{
        const criticism = await Criticism.create(req.body);
        return res.status(201).json({message: "Crítica publicada", criticism: criticism});
    }catch(err){
        res.status(500).json({error:err});
    }
};

const index = async(req,res) => {
    try{
        const criticism = await Criticism.findAll();
        return res.status(200).json({criticism});
    }catch(err){
            return res.status(500).json({err});
        }

};

const update = async(req,res) => {
    const {id} = req.params;
    try {
        const [updated] = await Criticism.update(req.body, {where: {id: id}});
        if(updated) {
            const criticism = await Criticism.findByPk(id);
            return res.status(200).send(criticism);
        } 
        throw new Error();
    }catch(err){
        return res.status(500).json("Não há crítica.");
    }
};

const destroy = async(req,res) => {
    const {id} = req.params;
    try {
        const deleted = await Criticism.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Crítica deletada!");
        }
        throw new Error ();
    }catch(err){
        return res.status(500).json("Não há crítica.");
    }
};

module.exports = {
    index,
    create,
    update,
    destroy
};