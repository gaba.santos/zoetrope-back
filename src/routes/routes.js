const { Router } = require('express');
const UserController = require('../controllers/UserController');
const ListController = require('../controllers/ListController');
const ListController = require('../controllers/CriticismController');
const router = Router();


router.get('/users',UserController.index);
router.post('/users',UserController.create);
router.put('/users/:id', UserController.update);
router.delete('/users/:id', UserController.destroy);

router.get('/users',ListController.index);
router.post('/users',ListController.create);
router.put('/users/:id', ListController.update);
router.delete('/users/:id', ListController.destroy);

router.get('/users',CriticismController.index);
router.post('/users',CriticismController.create);
router.put('/users/:id', CriticismtController.update);
router.delete('/users/:id', CriticismController.destroy);


module.exports = router;