const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const User = sequelize.define('User', {

    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    user_name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    phone_number: {
        type: DataTypes.STRING,
        allowNull: false
    },

    password: {
        type: DataTypes.STRING,
        allowNull: false
    },

    email: {
        type: DataTypes.STRING,
        allowNull: false
    },

    date_of_bith: {
        type: DataTypes.STRING,
        allowNull: false
    }

},{
    timestamps:false
});

User.associate = function(models) {
    User.hasMany(models.Criticism);
    User.belongsTo(models.List);
}

module.exports = User;