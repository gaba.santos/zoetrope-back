const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const List = sequelize.define('List', {

    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    type_serie_movie: {
        type: DataTypes.STRING,
        allowNull: false
    },

    gender: {
        type: DataTypes.STRING,
        allowNull: false
    },

    stars: {
        type: DataTypes.NUMBER,
        allowNull: false
    },

    age_classification: {
        type: DataTypes.STRING,
        allowNull: false
    }

},{
    timestamps:false
});

List.associate = function(models) {
    List.hasMany(models.User);
    
}

module.exports = List;