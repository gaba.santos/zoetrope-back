const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Criticism = sequelize.define('Criticism', {

    comment: {
        type: DataTypes.STRING,
        allowNull: false
    },

    date: {
        type: DataTypes.STRING,
        allowNull: false
    },

    hour: {
        type: DataTypes.STRING,
        allowNull: false
    },

    like: {
        type: DataTypes.NUMBER,
        allowNull: false
    }

},{
    timestamps:false
});

Criticism.associate = function(models) {
    Criticism.belongsTo(models.User);
    
}

module.exports = Criticism;